set(QSINGLEAPP_DIR ${CMAKE_CURRENT_LIST_DIR})
set(QSINGLEAPP_INCLUDE_DIRS  ${QSINGLEAPP_DIR})

set(QSINGLEAPP_SOURCES
        ${QSINGLEAPP_DIR}/QLocalPeer.h
        ${QSINGLEAPP_DIR}/QLocalPeer.cpp
        ${QSINGLEAPP_DIR}/QLockedFile.h
        ${QSINGLEAPP_DIR}/QLockedFile.cpp
        ${QSINGLEAPP_DIR}/QSingleApplication.h
        ${QSINGLEAPP_DIR}/QSingleApplication.cpp
        ${QSINGLEAPP_DIR}/QSingleCoreApplication.h
        ${QSINGLEAPP_DIR}/QSingleCoreApplication.cpp
    )

if(WIN32)
    set(QSINGLEAPP_SOURCES ${QSINGLEAPP_SOURCES} 
          ${QSINGLEAPP_DIR}/QLockedFileWin.cpp)
else()
    set(QSINGLEAPP_SOURCES ${QSINGLEAPP_SOURCES} 
           ${QSINGLEAPP_DIR}/QLockedFileUnix.cpp)
endif()

set(QSINGLEAPP_FILES ${QSINGLEAPP_SOURCES})
